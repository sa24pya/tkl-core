<?php
/**
 * File: Mongo/IndexManipulator.php
 *
 * @author mrx, Etor, Victor
 * @package Core
 * @subpackage Mongo
 * @version 1.0.1
 */

namespace Core\Mongo;

use MongoDB\Driver as MongoDriver;


/**
 * IndexManipulator
 *
 * @author  victor
 *
 * @package Core
 * @subpackage Mongo
 * @version 1.0.1
 */
class IndexManipulator
{
    protected $logger = null;

    /**
     * __construct(): Attach the logger to the class
     */
    public function __construct($logger = null)
    {
        if(!empty($logger)) {
            $this->logger = $logger;
            $this->logger->info('index manipulator init...');
        }
    }

    /**
     * createCommand(): Generates a new command
     * @param array $options : Command object options
     * @return MongoDB\Driver\Command instance
     */
    public function createCommand(array $options)
    {
        return new MongoDriver\Command($options);
    }

    /**
     * generateSparseKey()
     * @param array $keys : The index keys
     * @param type|string $name : The index name, by default a random string
     * @return Object
     */
    public function generateSparseKey(array $keys, $name = null)
    {
        // Generate a random index name:
        if (!$name) {
            $name = base64_encode(random_bytes(48));
        }
        return [
            "key" => $keys,
            "background" => true,
            "name" => $name,
            "sparse" => true,
        ];
    }

    public function generateGS1($collectionName)
    {
        $child = "catalogueItemChildItemLink.catalogueItem";
        $gln = "tradeItem.informationProviderOfTradeItem.gln";
        $gtin = "tradeItem.gtin";
        return $this->createCommand(
            [
            'createIndexes' => $collectionName,
            'indexes' => [
                [
                    "key" => [ "catalogueItem.$gtin" => 1 ],
                    "background" => true,
                    "name" => "gs1_gtinIndex1",
                    "unique" => true,
                ],
                [
                    "key" => [ "catalogueItem.$gln" => 1 ],
                    "background" => true,
                    "name" => "gs1_glnIndex1",
                ],
                $this->generateSparseKey([
                    "catalogueItem.$child.$gtin" => 1,
                    ], "gs1_gtinIndex2"),
                $this->generateSparseKey([
                    "catalogueItem.$child.$gln" => 1,
                    ], "gs1_glnIndex2"),
                $this->generateSparseKey([
                        "catalogueItem.$child.$child.$gtin" => 1,
                    ], "gs1_gtinIndex3"),
                $this->generateSparseKey([
                        "catalogueItem.$child.$child.$gln" => 1,
                    ], "gs1_glnIndex3"),
                $this->generateSparseKey([
                        "catalogueItem.$child.$child.$child.$gtin" => 1,
                    ], "gs1_gtinIndex4"),
                $this->generateSparseKey([
                        "catalogueItem.$child.$child.$child.$gln" => 1,
                    ], "gs1_glnIndex4"),
                ],
            ]
        );
    }

    public function generateSA24($collectionName)
    {
        $gln = "tradeItem.informationProviderOfTradeItem.gln";
        $gtin = "tradeItem.gtin";
        return $this->createCommand(
            [
            'createIndexes' => $collectionName,
            'indexes' => [
                [
                    "key" => [ "catalogueItem.$gtin" => 1 ],
                    "background" => true,
                    "name" => "sa24_gtinIndex",
                    "unique" => true,
                ],
                [
                    "key" => [ "catalogueItem.$gln" => 1 ],
                    "background" => true,
                    "name" => "sa24_glnIndex",
                ],
                [
                    "key" => [ "catalogueItem.tradeItem.ean" => 1 ],
                    "background" => true,
                    "name" => "sa24_eanIndex",
                    "unique" => true,
                    "sparse" => true,
                ],
                [
                    "key" => [ "catalogueItem.tradeItem.additionalTradeItemIdentification.@value" => 1 ],
                    "background" => true,
                    "name" => "sa24_articleNumberIndex",
                    "unique" => true,
                    "sparse" => true,
                ],
                ],
            ]
        );
    }
    /**
     * createIndexes(): Generate the collection needed indexes.
     * @param array $config : The config file
     */
    public function createIndexes($config)
    {
        if (!$config) {
            return false;
        }
        // Mongo connection
        $mongoServer = $config['mongoServer'];
        $mongoPort = $config['mongoPort'];
        $manager = new MongoDriver\Manager("mongodb://$mongoServer:$mongoPort");
        $mongoDBName = $config['mongoDBName'];
        // Collection name
        $mongoCollectionName = $config['mongoCollectionName'];
        $mongoCollectionSA24Name = $config['mongoCollectionSA24Name'];
        if (!$this->dropIndexes($config)) {
            return false;
        }
        $gs1Indexes = $this->generateGS1($mongoCollectionName);
        $sa24Indexes = $this->generateSA24($mongoCollectionSA24Name);
        try {
            $manager->executeCommand($mongoDBName, $gs1Indexes);
            $manager->executeCommand($mongoDBName, $sa24Indexes);
            if(!empty($logger)) {
                $this->logger->info('Created new indexes...');
            }
            return true;
        } catch (\Exception $e) {
            if(!empty($logger)) {
                $this->logger->error('MongoDB Exception: '.
                    $e->getMessage());
            }
            return false;
        }
    }

    /**
     * dropIndexes(): Drops all indexes
     * @param array $config : The settings array
     * @return bool : If removed successfully.
     */
    public function dropIndexes($config)
    {
        if (!$config) {
            return false;
        }
        // Mongo connection
        $mongoServer = $config['mongoServer'];
        $mongoPort = $config['mongoPort'];
        $manager = new MongoDriver\Manager("mongodb://$mongoServer:$mongoPort");
        $mongoDBName = $config['mongoDBName'];
        // Collection name
        $mongoCollectionName = $config['mongoCollectionName'];
        $mongoCollectionSA24Name = $config['mongoCollectionSA24Name'];
        $removeGS1IndexesCommand = $this->createCommand(
            array('dropIndexes' => $mongoCollectionName,
                'index' => '*',
            )
        );
        $removeSA24IndexesCommand = $this->createCommand(
            array('dropIndexes' => $mongoCollectionSA24Name,
                'index' => '*',
            )
        );
        try {
            $manager->executeCommand($mongoDBName, $removeSA24IndexesCommand);
            $manager->executeCommand($mongoDBName, $removeGS1IndexesCommand);
             if(!empty($logger)) {
                $this->logger->info('Deleted old indexes...');
            }
            return true;
        } catch (\Exception $e) {
             if(!empty($logger)) {
                $this->logger->error('MongoDB Exception: '.
                    $e->getMessage());
            }
            return false;
        }
    }
}
