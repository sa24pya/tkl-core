<?php
/**
 * File: CollectionCreator.php
 *
 * @author mrx, anonymous
 * 
 * @package Core
 * @subpackage Mongo
 * @version 1.0.6
 */
namespace Core\Mongo;


/**
 * Class CollectionCreator
 *
 * @package: Core
 * @subpackage Mongo
 * @version: 1.0.6
 */
class CollectionCreator 
{
    protected $cmd = array();

    function __construct($collectionName) {
        $this->cmd["create"] = (string)$collectionName;
    }
    function setAutoIndexId($bool) {
        $this->cmd["autoIndexId"] = (bool)$bool;
    }
    function setCappedCollection($maxBytes, $maxDocuments = false) {
        $this->cmd["capped"] = true;
        $this->cmd["size"]   = (int)$maxBytes;

        if ($maxDocuments) {
            $this->cmd["max"] = (int)$maxDocuments;
        }
    }
    function usePowerOf2Sizes($bool) {
        if ($bool) {
            $this->cmd["flags"] = 1;
        } else {
            $this->cmd["flags"] = 0;
        }
    }
    function setFlags($flags) {
        $this->cmd["flags"] = (int)$flags;
    }
    function getCommand() {
        return new \MongoDB\Driver\Command($this->cmd);
    }
    function getCollectionName() {
        return $this->cmd["create"];
    }
}
