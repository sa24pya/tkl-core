<?php
/**
 * File: Tool/ListXMLFiles.php
 *
 * @author mrx, Francescm, Minh
 * 
 * @package Core
 * @subpackage Tool
 * @version 1.0.1
 */

namespace Core\Tool;

/**
 * Class ListXMLFiles
 *
 * return all the files (not directories) of a directory which
 * have .xml or .XML extension
 *
 * usage:
 * ListXMLFiles::getList("folderName");
 *
 * @author mrx, Francescm, Minh
 *
 * @package Core
 * @subpackage Tool
 * @version 1.0.1
 */
class ListXMLFiles {

  static private function filter(string $file) {
    if( is_file( $file ) ) {
        return $file;
    }
    return "";
  }

  static public function getList( string $directory )
  {
    $files = scandir($directory);

    //Sort list file by properties creation date in OS by ASC (not in file name)
    $listfile = array();
    $data =array();
    for( $i = 0 ; $i < count($files) ; $i++ ) {
      if(($files[$i] != ".") && ($files[$i] != "..")){
        $files[$i] = $directory . DIRECTORY_SEPARATOR . $files[$i];
        $listfile[$files[$i]] = date ("Y-m-d H:i:s.", filemtime($files[$i]));
      }
    }
    asort($listfile);

    $j = 0;
    foreach($listfile as $key=>$value)
    {
      $data[$j] = $key;
      $j++;
    }

    $files = array_map( "self::filter", $data );
    
    return preg_grep("/(.xml)|(.XML)$/", $files);
  }
}
