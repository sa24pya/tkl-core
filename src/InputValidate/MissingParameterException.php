<?php
/**
 * File: MissingParameterException.php
 *
 *
 * @author Francesc Roma Frigole cesc2010@cesc.cat
 * 
 * @version 1.0
 * @package Core
 * @subpackage InputValidate
 *  
 */

namespace Core\InputValidate;

/**
 * Class MissingParameterException
 *
 * extends Exception to provide a way for tests to distinguish when the class
 * execution fails due to missing paramiter
 *
 * @author Francesc Roma Frigole cesc2010@cesc.cat
 *
 * @version 1.0
 * @package Core
 * @subpackage InputValidate
 */

class MissingParameterException extends \Exception
{
    // Redefine the exception to have add the inut name to a predifined message
    public function __construct($inputName, $code = 0, \Exception $previous = null) {
        // some code

        // make sure everything is assigned properly
        parent::__construct('Missing input parameter ' . $inputName, $code, $previous);
    }

    // custom string representation of object
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
};
