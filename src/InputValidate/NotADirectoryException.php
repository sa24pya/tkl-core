<?php
/**
 * File: NotADirectoryException.php
 *
 *
 * @author Francesc Roma Frigole cesc2010@cesc.cat
 * 
 * @version 1.0
 * @package Core
 * @subpackage InputValidate
 *  
 */

namespace Core\InputValidate;

/**
 * Class NotADirectoryException
 *
 * extends Exception to provide a way for tests to distinguish when the class
 * execution fails due to an input directory parameter not corresponding to
 * an actual directory in the file system (not existing or not being a
 * directory)
 *
 * @author Francesc Roma Frigole cesc2010@cesc.cat
 *
 * @version 1.0
 * @package Core
 * @subpackage InputValidate
 */
class NotADirectoryException extends \Exception
{
    // Redefine the exception to have add the inut name to a predifined message
    public function __construct($inputName, $code = 0, \Exception $previous = null) {
        // some code

        // make sure everything is assigned properly
        parent::__construct("Directory $inputName doesn't exist", $code, $previous);
    }

    // custom string representation of object
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
};
