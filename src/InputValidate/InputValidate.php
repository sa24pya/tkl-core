<?php
/**
 * File: InputValidate.php
 *
 *
 * @author Francesc Roma Frigole cesc2010@cesc.cat
 * 
 * @version 1.0
 * @package Core
 * @subpackage InputValidate
 *  
 */

namespace Core\InputValidate;

use Core\InputValidate\MissingParameterException;
use Core\InputValidate\NotADirectoryException;

/**
 * Class InputValidate
 *
 * extends Exception to provide a way for tests to distinguish when the class
 * execution fails due to an input directory parameter not corresponding to
 * an actual directory in the file system (not existing or not being a
 * directory)
 * 
 * @author Francesc Roma Frigole cesc2010@cesc.cat
 *
 * @package Core
 * @subpackage InputValidate
 * 
 */
class InputValidate
{

  /**
   * It checks that all the required configuration parameters exist
   *
   * @config associative array of configuration to be validated
   * @configKeys array of required parameters
   *
   * @return void
   * @throws MissingParameterException if one of the required parameters is
   *         Missing
   */
  static function config(array $config, array $configKeys )
  {
      foreach ($configKeys as $key) {
          if (!array_key_exists($key, $config) || is_null($key)) {
              throw new MissingParameterException( $key );
          }
      }
  }

  /**
   * It checks that all the required directory parameters exist
   *
   * @directories array of directories to be validated
   *
   * @return void
   * @throws NotADirectoryException if one of the required directories directories
   *         not exist
   */

  static function directories(array $directories )
  {
      foreach ($directories as $directory) {

        $path = realpath($directory);
        if (!is_dir($path)) {
          throw new NotADirectoryException( $directory );
        }
      }
  }
}
