<?php
/**
 * File: tests/InputValidateTest.php
 *
 * @author mrx <silentstar@riseup.net>
 *
 * @package Core
 * @subpackage InputValidate
 * @version 1.0.0
 *
 */

namespace Core\InputValidate;

/**
 * Class DALTest
 *
 * @package Core
 * @subpackage InputValidate
 * @version 1.0.0
 */
class InputValidateTest extends \PHPUnit_Framework_TestCase
{
    /**
     * App environemnt has impact on congif values
     *
     * @var string
     */
    const APP_ENV = "development";

    public function setUp() {}
    public function tearDown() {}


    /**
     * Test collection exists
     */
    public function testCanConstruct(){
      $iv = new InputValidate();
      /** @todo check the class name is proper */
      $this->assertTrue(!empty($iv));
    }
}
