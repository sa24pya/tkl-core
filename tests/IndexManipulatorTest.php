<?php
/**
 * File: tests/IndexManipulatorTest.php
 *
 * @author mrx <silentstar@riseup.net>
 *
 * @package Core
 * @subpackage Mongo
 * @version 1.0.1
 *
 */

namespace Core\Mongo;

use DataAccessDAL\DAL;
use Dotenv\Dotenv;

// Log
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

/**
 * Class IndexManipulatorTest
 *
 * @package Core
 * @subpackage Mongo
 * @version 1.0.1
 */
class IndexManipulatorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * App environemnt has impact on congif values
     *
     * @var string
     */
    const APP_ENV = "development";

    /**
     * logger
     *
     * @var object Monolog\Logger
     */
    private $logger;


    public function setUp()
    {
        $dotenv = new Dotenv(dirname(__DIR__));
        $dotenv->load();

        // Log
        $this->logger = new Logger('core-mongo-indexmanipulator');
        $this->logger->pushHandler(new StreamHandler(getenv('PATH_TO_LOG_FILE'), Logger::INFO));


        $names = [];
        $collections = DAL::getInstance()->getCollectionNames();
        foreach ($collections as $collection) {
            $names []= $collection->name;
        }
        if (!in_array('GS1_test', $names)) {
            DAL::getInstance()->createCollection('GS1_test');
        }
        if (!in_array('SA24_test', $names)) {
            DAL::getInstance()->createCollection('SA24_test');
        }
    }

    public function tearDown()
    {
        DAL::getInstance()->dropCollection('GS1_test');
        DAL::getInstance()->dropCollection('SA24_test');
    }

    /**
     * Test collection exists
     */
    public function testCanConstruct()
    {
        $o = new IndexManipulator($this->logger);
        /** @todo check the class name is proper */
        $this->assertTrue(!empty($o));
    }

    public function test()
    {
        //Import config:
        /*         $mongoServer = $config['mongoServer'];
        $mongoPort = $config['mongoPort'];
        $manager = new MongoDriver\Manager("mongodb://$mongoServer:$mongoPort");
        $mongoDBName = $config['mongoDBName'];
        // Collection name
        $mongoCollectionName = $config['mongoCollectionName'];
        $mongoCollectionSA24Name = $config['mongoCollectionSA24Name']; */
        $config = [
            'mongoServer' => 'localhost',
            'mongoPort' => '27017',
            'mongoDBName' => 'SA24',
            'mongoCollectionName' => 'GS1_test',
            'mongoCollectionSA24Name' => 'SA24_test',
        ];

        $idxc = new IndexManipulator($this->logger);
        // Should return false if the config is missing:
        $this->assertFalse(
            $idxc->dropIndexes(null)
        );
        $this->assertFalse(
            $idxc->createIndexes(null)
        );

        // Should return true if succeded:
        $this->assertTrue(
            $idxc->dropIndexes($config)
        );
        $this->assertTrue(
            $idxc->createIndexes($config)
        );
    }
}
