<?php
/**
 * File: tests/ListXMLFilesTest.php
 *
 * @author mrx <silentstar@riseup.net>
 *
 * @package Core
 * @subpackage Tool
 * @version 1.0.4
 *
 */

namespace Core\Tool;

/**
 * Class DALTest
 *
 * @package Core
 * @subpackage Tool
 * @version 1.0.0
 */
class ListXMLFilesTest extends \PHPUnit_Framework_TestCase
{
    /**
     * App environemnt has impact on congif values
     *
     * @var string
     */
    const APP_ENV = "development";

    public function setUp() {}
    public function tearDown() {}


    /**
     * Test collection exists
     */
    public function testCanConstruct(){
      $o = new ListXMLFiles();
      /** @todo check the class name is proper */
      $this->assertTrue(!empty($o));
    }
}
