<?php
/**
 * File: tests/XML2ArrayTest.php
 *
 * @author mrx <silentstar@riseup.net>
 *
 * @package Core
 * @subpackage Mongo
 * @version 1.0.4
 *
 */

namespace Core\Mongo;

/**
 * Class CollectionCreatorTest
 *
 * @package Core
 * @subpackage mongo
 * @version 1.0.4
 */
class CollectionCreatorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * App environemnt has impact on congif values
     *
     * @var string
     */
    const APP_ENV = "development";

    public function setUp() {}
    public function tearDown() {}


    /**
     * Test collection exists
     */
    public function testCanConstruct(){
      $o = new CollectionCreator('test');
      /** @todo check the class name is proper */
      $this->assertTrue(!empty($o));
    }
}
