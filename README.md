# Taklia Core Library Package
TKL Core  
TKL Core provides basic library.  
Support PSR-1, PSR-2, PSR-3, PSR-4.

![tkl package repository](https://www.dream-destination-wedding.com/images/exotic2.jpg)

## Install

Via Composer

``` bash
$ composer require tkl/core
```

## Usage

``` bash
$ 
$ 
```

## Testing

``` bash
$ phpunit
```

## Contributing

Please see [CONTRIBUTING](https://github.com/sa42/core/blob/master/CONTRIBUTING.md) for details.

## Credits

- [Wilson Smith](https://github.com/wilsonsmith)
- [All Contributors](https://github.com/sa24/core/contributors)

## License

The GNU General Public Licence version 3 (GPLv3). Please see [License File](LICENSE.md) for more information.
